import { NextFunction, Request, Response } from "express";

export interface IArtist {
    id: string;
    first_name: string;
    last_name: string;
    nickname?: string;
    email?: string;
    songs?: ISong[];
    about?: string;
}

export interface ISong {
    id: string;
    name: string;
    time: string;
    owner: string;
    playlists?: string[];
    // writers?: string[];
    // label?: string;
    lyrics?: string;
}
export interface IPlaylist {
    id: string;
    name: string;
    total_time?: string;
    songs?: ISong[];
    number_of_songs?: number;
    created_by?: string;
}
export interface IUser {
    id: string;
    first_name: string;
    last_name: string;
    email: string;
    // username: string;
    password?: string;
    // phone: string;
    playlists?: IPlaylist[];
    refresh_token?: string;
}

export interface IResponseMessage {
    status: number;
    message: string;
    data: any;
}
export interface IErrorResponse {
    status: number;
    message: string;
    stack?: string;
}

export type middleWareFunction = (
    req: Request,
    res: Response,
    next: NextFunction
) => any;
