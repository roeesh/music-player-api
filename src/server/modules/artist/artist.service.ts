import { IArtist, ISong } from "../../../types/types.js";
import artist_model from "./artist.model.js";
import song_model from "../song/song.model.js";
import { deleteSongByID } from "../song/song.service.js";

// Create new artist
export async function createNewArtist(reqBody: IArtist): Promise<any> {
    const artist = await artist_model.create(reqBody);
    return artist;
}

// Get all artists
export async function getAllArtists(): Promise<any> {
    const artists = await artist_model.find().select(`-_id 
                                                first_name 
                                                last_name
                                                nickname 
                                                email
                                                about 
                                                songs`);
    return artists;
}

// Get all artists - with pagination
export async function getAllArtistsWithPagination(
    page: string,
    limit: string
): Promise<any> {
    const artists = await artist_model
        .find()
        .skip(Number(page) * Number(limit))
        .limit(Number(limit)).select(`-_id 
                                        first_name 
                                        last_name
                                        nickname 
                                        email
                                        about 
                                        songs`);
    return artists;
}

// Get a single artist
export async function getArtistByID(idToRead: string): Promise<any> {
    const artist = await artist_model.findById(idToRead);
    return artist;
}

// Update a artist using the id
export async function updateArtist(
    idToUpdate: string,
    reqBody: IArtist
): Promise<any> {
    const artist = await artist_model.findByIdAndUpdate(idToUpdate, reqBody, {
        new: true,
        upsert: false,
    });
    return artist;
}

// Delete a artist using the id
export async function deleteArtistByID(idToDelete: string): Promise<any> {
    // artistMongoAdapter(deleteArtist(idToDelete));
    const artist = await artist_model.findByIdAndRemove(idToDelete);

    artist.songs.forEach(async (song: ISong) => {
        //Delete each song of this artist
        await deleteSongByID(song.id);
    });
    return artist;
}

// Get artist by song id
export async function getArtistBySongID(songID: string): Promise<any> {
    const artist = await song_model.findById(songID).populate("owner")
        .select(`-_id 
                                                                                first_name 
                                                                                last_name
                                                                                nickname 
                                                                                email
                                                                                about 
                                                                                songs`);
    return artist;
}

// **** helper functions for song service ****

// update song in Artist.songs by artist ID
export async function updateArtistSongsByArtistID(
    artistID: string,
    songIDToUpdate: string,
    song: ISong
) {
    const artistOwner = await artist_model.findById(artistID); // update Artist.songs
    const songIndexToUpdate: number = artistOwner.songs.findIndex(
        (song: ISong) => song.id === songIDToUpdate
    );
    artistOwner.songs.splice(songIndexToUpdate, 1, song);
    await artistOwner.save();
}

// Delete song from Artist.songs by artist ID
export async function deleteArtistSongsByArtistID(
    artistID: string,
    songIDToDelete: string
) {
    const artistOwner = await artist_model.findById(artistID); // update Artist.songs
    const songIndexToDelete: number = artistOwner.songs.findIndex(
        (song: ISong) => song.id === songIDToDelete
    );
    artistOwner.songs.splice(songIndexToDelete, 1);
    await artistOwner.save();
}
