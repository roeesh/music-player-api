import bcrypt from "bcrypt";
import { IUser } from "../../../types/types.js";
import user_model from "./user.model.js";
import playlist_model from "../playlist/playlist.model.js";
import pkg from "bluebird";
import { deletePlaylistByID } from "../playlist/playlist.service.js";
const { Promise } = pkg;

// Create new user
export async function createNewUser(reqBody: IUser): Promise<any> {
    reqBody.password = await bcrypt.hash(reqBody.password as string,10);
    console.log({reqBody});
    const user = await user_model.create(reqBody);
    return user;
}

// Get all users
export async function getAllUsers(): Promise<any> {
    const users = await user_model.find().select(`-_id 
                                                first_name 
                                                last_name 
                                                email `);
    return users;
}

// Get all users - with pagination
export async function getAllUsersWithPagination(
    page: string,
    limit: string
): Promise<any> {
    const users = await user_model
        .find()
        .skip(Number(page) * Number(limit))
        .limit(Number(limit)).select(`-_id 
                                          first_name 
                                          last_name 
                                          email`);
    return users;
}

// Get a single user
export async function getUserByID(idToRead: string): Promise<any> {
    const user = await user_model.findById(idToRead).populate("playlists").select(`-_id 
                                                                                    first_name 
                                                                                    last_name 
                                                                                    email
                                                                                    refresh_token `);
    return user;
}

// Get a single user by email and password
export async function getUserByEmail(
    email: string,
    // password: string
): Promise<any> {
    const user = await user_model.findOne({ "email": email });
    return user;
}

// Update a user using the id
export async function updateUser(
    idToUpdate: string,
    reqBody: IUser | any
): Promise<any> {
    const user = await user_model.findByIdAndUpdate(idToUpdate, reqBody, {
        new: true,
        upsert: false,
    }).select(`-_id 
              first_name 
              last_name 
              email `);
    return user;
}

// Delete a user using the id
export async function deleteUserByID(idToDelete: string): Promise<any> {
    const user = await user_model.findById(idToDelete);

    Promise.each(user.playlists, async (playlistID: string) => {
        // Delete each playlist of this user
        await deletePlaylistByID(playlistID);
    });

    await user_model.findByIdAndRemove(idToDelete).select(`-_id 
                                                        first_name 
                                                        last_name 
                                                        email `);
    return user;
}

// Get user by playlist id
export async function getUserByPlaylistID(playlistID: string): Promise<any> {
    const user = await playlist_model
        .findById(playlistID)
        .populate("created_by").select(`-_id 
                                        first_name 
                                        last_name 
                                        email `);
    return user;
}

// **** helper functions for playlist service ****

// Delete playlist from User.playlists by user ID
export async function deletePlaylistFromUser(
    userID: string,
    playlistIDToDelete: string
) {
    const playlistCreator = await user_model.findById(userID); // update user.playlists
    if (playlistCreator !== null) {
        const playlistIndexToDelete: number =
            playlistCreator.playlists.findIndex(
                (playlistID: string) => playlistID === playlistIDToDelete
            );
        playlistCreator.playlists.splice(playlistIndexToDelete, 1);
        await playlistCreator.save();
    }
}
