import { IPlaylist, ISong } from "../../../types/types.js";
import playlist_model from "../playlist/playlist.model.js";
import song_model from "../song/song.model.js";
import pkg from "bluebird";
import { deletePlaylistFromSong } from "../song/song.service.js";
import user_model from "../user/user.model.js";
import { deletePlaylistFromUser } from "../user/user.service.js";
import HttpException from "../../exceptions/http-exception.js";

const { Promise } = pkg;
export const SONG_EXISTS = "song already exists in this playlist";

// Create new playlist
export async function createNewPlaylist(
    reqBody: IPlaylist,
    creatorID: string
): Promise<any> {
    const creator = await user_model.findById(creatorID);
    if (creator === null) {
        return undefined;
    }
    const playlist = await playlist_model.create(reqBody);
    console.log("id of plalyst: ", playlist.id);
    const playlistToUpdate = await playlist_model.findById(playlist.id);
    playlistToUpdate.created_by = creatorID;
    // await playlist_model.findOneAndUpdate({ id: playlist.id }, { created_by: creatorID });
    playlistToUpdate.save();
    // insert playlist to user's playlists list
    creator.playlists.push(playlist);
    creator.save();

    return playlist;
}

// Get all playlists
export async function getAllplaylists(): Promise<any> {
    const playlists = await playlist_model.find().select(`-_id 
                                                name
                                                total_time
                                                created_by
                                                songs
                                                number_of_songs`);
    return playlists;
}

// Add a song to playlist (and update song.playlists field and playlist.total_time & playlist.number_of_songs fields)
export const addSongToPlaylist = async (playlistID: string, songID: string, userID: string) => {
    const playlist = await playlist_model.findById(playlistID);

    if (playlist === null) {
        return undefined;
    }
    const song = await song_model.findById(songID);
    if (song === null) {
        return undefined;
    }
    // Only playlist creator can add songs to his playlist
    if(playlist.created_by != userID) {
        throw new HttpException(
            403,
            "You are not allowed to add song to this playlist."
        );
    }
    

    // add song to playlist
    if (!playlist.songs.some((song: ISong) => song.id === songID)) {
        playlist.songs.push(song);

        // update playlist.number_of_songs
        playlist.number_of_songs = playlist.songs.length;

        // update playlist.total_time
        const songTime: string = song.time;
        if (!playlist.total_time) {
            playlist.total_time = songTime;
        } else {
            const toalPlaylistTime = addSongTimeToPlaylistTotalTime(
                songTime,
                playlist.total_time
            );
            playlist.total_time = toalPlaylistTime;
        }
        await playlist.save();
    } else {
        return SONG_EXISTS;
    }

    // update song.playlists
    if (!song.playlists.includes(playlistID)) {
        song.playlists.push(playlist);
        await song.save();
    }
    return playlist;
};

// Get a single playlist
export async function getPlaylistByID(idToRead: string): Promise<any> {
    const playlist = await playlist_model.findById(idToRead);
    return playlist;
}

// Update a playlist using the id
export async function updatePlaylist(
    idToUpdate: string,
    reqBody: IPlaylist,
    userID: string
): Promise<any> {
    const playlistToCheck = await playlist_model.findById(idToUpdate);
    // Only playlist creator can update his playlist
    if(playlistToCheck.created_by != userID) {
        throw new HttpException(
            403,
            "You are not allowed to update this playlist."
        );
    }

    const playlist = await playlist_model.findByIdAndUpdate(
        idToUpdate,
        reqBody,
        { new: true, upsert: false }
    );
    return playlist;
}

// Delete a playlist using the id (and delete it also from each song)
// Using function overload to support delete requests from user (checks if the userID = playlist.created_by)
export async function deletePlaylistByID(idToDelete: string): Promise<any>;
export async function deletePlaylistByID(idToDelete: string, userID: string): Promise<any>;
export async function deletePlaylistByID(arg1: string, arg2?: string): Promise<any> {
    if(arg2 !== undefined){ //got userID from delete request by user
        const playlistToCheck = await playlist_model.findById(arg1);
        // Only playlist creator can delete his playlist
        if(playlistToCheck.created_by != arg2) {
            throw new HttpException(
                403,
                "You are not allowed to delete this playlist."
            );
        }
    }
    const playlist = await playlist_model.findByIdAndRemove(arg1);
    if (playlist === null) {
        return undefined;
    }

    // delete playlist from each song in which the playlist in (Song.Playlists)
    Promise.each(playlist.songs, async (song: ISong) => {
        await deletePlaylistFromSong(arg1, song);
    });

    // delete playlist from the user which created it (user.Playlists)
    await deletePlaylistFromUser(playlist.created_by, arg1);

    return playlist;
}

// **** helper functions for song service ****

// update song in each playlist in which the song in (Playlist.songs)
export async function updatePlaylistOfSongByPlaylistID(
    playlistID: string,
    songIDToUpdate: string,
    song: ISong,
    oldSong: ISong
) {
    const playlistOfSong = await playlist_model.findById(playlistID);

    // update playlist total time
    const totalPlaylistTimeWithoutOldSong =
        substructSongTimeFromTotalPlaylistTime(
            playlistOfSong.total_time,
            oldSong.time
        );
    const toalPlaylistTime = addSongTimeToPlaylistTotalTime(
        song.time,
        String(totalPlaylistTimeWithoutOldSong)
    );
    playlistOfSong.total_time = toalPlaylistTime;

    // update the song in his playlist
    const songIndexToUpdate: number = playlistOfSong.songs?.findIndex(
        (song: ISong) => song.id === songIDToUpdate
    );
    playlistOfSong.songs.splice(songIndexToUpdate, 1, song);
    await playlistOfSong.save();
}

// delete song in each playlist in which the song in (Playlist.songs)
export async function deletePlaylistOfSongByPlaylistID(
    playlistID: string,
    songIDToDelete: string,
    song: ISong
) {
    const playlistOfSong = await playlist_model.findById(playlistID);

    // delete the song in his playlist
    const songIndexToDelete: number = playlistOfSong.songs?.findIndex(
        (song: ISong) => song.id === songIDToDelete
    );
    playlistOfSong.songs.splice(songIndexToDelete, 1);

    // update playlist.number_of_songs
    playlistOfSong.number_of_songs = playlistOfSong.songs.length;
    if (playlistOfSong.number_of_songs === 0) {
        // if no songs in the playlist - delete it
        await deletePlaylistByID(playlistOfSong.id);
        return;
    }

    // update playlist total time
    const toalPlaylistTime = substructSongTimeFromTotalPlaylistTime(
        playlistOfSong.total_time,
        song.time
    );
    playlistOfSong.total_time = toalPlaylistTime;

    await playlistOfSong.save();
}

// **** Helper functions for playlist sercive ****

// Add Song Time To Total Playlist Time
function addSongTimeToPlaylistTotalTime(
    songTime: string,
    playlistTime: string
): string {
    const songTimeArr: string[] = songTime.split(":");
    const [minutesOfSong, secondsOfSong] = songTimeArr;
    const playlistTimeArr: string[] = playlistTime.split(":");
    const [minutesOfPlaylist, secondsOfPlaylist] = playlistTimeArr;
    let totalMinutes = Number(minutesOfSong) + Number(minutesOfPlaylist);
    let totalSeconds = Number(secondsOfSong) + Number(secondsOfPlaylist);
    if (totalSeconds >= 60) {
        totalSeconds = Number(totalSeconds) - 60;
        totalMinutes += 1;
    }
    const toalPlaylistTime = `${totalMinutes}:${totalSeconds}`;
    return toalPlaylistTime;
}

// Substruct Song Time From Total Playlist Time
function substructSongTimeFromTotalPlaylistTime(
    playlistTime: string,
    songTime: string
): string {
    const songTimeArr: string[] = songTime.split(":");
    const [minutesOfSong, secondsOfSong] = songTimeArr;
    const playlistTimeArr: string[] = playlistTime.split(":");
    const [minutesOfPlaylist, secondsOfPlaylist] = playlistTimeArr;
    let totalMinutes = Number(minutesOfPlaylist) - Number(minutesOfSong);
    let totalSeconds = Number(secondsOfPlaylist) - Number(secondsOfSong); //-10
    if (totalSeconds < 0) {
        totalSeconds = Number(totalSeconds) + 60;
        totalMinutes -= 1;
    }
    const toalPlaylistTime = `${totalMinutes}:${totalSeconds}`;
    return toalPlaylistTime;
}
