import mongoose from "mongoose";
const { Schema, model } = mongoose;

export const SongSchema = new Schema(
    {
        name: { type: String, required: true },
        time: { type: String, required: true },
        owner: { type: Schema.Types.ObjectId, ref: "artist", require: true },
        playlists: [{ type: Schema.Types.ObjectId, ref: "playlist" }],
        // writers: [ { type: Schema.Types.ObjectId, ref:"artist"} ],
        // label: { type : String },
        lyrics: { type: String },
    },
    { timestamps: true }
);

export default model("song", SongSchema);
