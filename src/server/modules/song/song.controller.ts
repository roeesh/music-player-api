import express, { Request, Response } from "express";
import { IResponseMessage, ISong } from "../../../types/types.js";
import HttpException from "../../exceptions/http-exception.js";
import raw from "../../middleware/route.async.wrapper.js";
import { validateBeforeOperationMW } from "../../middleware/validations/schema.validation.js";
import { schemaForCreateSong, schemaForUpdateSong } from "./song.validation.js";
import {
    createNewSong,
    deleteSongByID,
    getAllASongs,
    getArtistsSongsByID,
    getSongByID,
    NO_OWNER,
    updateSong,
} from "./song.service.js";

const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

router
    .route("/")
    // Creates a new song (use validation MW before)
    .post(
        raw(validateBeforeOperationMW(schemaForCreateSong)),
        raw(async (req: Request, res: Response) => {
            const song = await createNewSong(req.body as ISong, req.body.owner);
            if (song === NO_OWNER) {
                throw new HttpException(
                    400,
                    `No such artist with id: ${req.body.owner}`
                );
            }
            const outputResponse: IResponseMessage = {
                status: 200,
                message: "Song created",
                data: song,
            };
            res.status(outputResponse.status).json(outputResponse);
        })
    )
    // Get all songs
    .get(
        raw(async (req: Request, res: Response) => {
            const songs = await getAllASongs();
            const outputResponse: IResponseMessage = {
                status: 200,
                message: "All existing songs",
                data: songs,
            };
            res.status(outputResponse.status).json(outputResponse);
        })
    );

// Get all songs of an artist by ID
router.get(
    "/artist/:id",
    raw(async (req, res) => {
        const songsOfArtist = await getArtistsSongsByID(req.params.id);
        if (!songsOfArtist) {
            throw new HttpException(
                400,
                `No such artist with id: ${req.params.id}`
            );
        } else {
            const outputResponse: IResponseMessage = {
                status: 200,
                message: `All songs of the artist with id: ${req.params.id}`,
                data: songsOfArtist,
            };
            res.status(outputResponse.status).json(outputResponse);
        }
    })
);

router
    .route("/:id")
    // Gets a single song by ID
    .get(
        raw(async (req: Request, res: Response) => {
            const song = await getSongByID(req.params.id);
            if (!song) {
                throw new HttpException(
                    400,
                    `No such song with id: ${req.params.id}`
                );
            } else {
                const outputResponse: IResponseMessage = {
                    status: 200,
                    message: "song found",
                    data: song,
                };
                res.status(outputResponse.status).json(outputResponse);
            }
        })
    )
    // Updates a single song by ID (use validation MW before)
    .put(
        raw(validateBeforeOperationMW(schemaForUpdateSong)),
        raw(async (req: Request, res: Response) => {
            const song = await updateSong(req.params.id, req.body as ISong);
            if (!song) {
                throw new HttpException(
                    400,
                    `No such song with id: ${req.params.id}`
                );
            } else {
                const outputResponse: IResponseMessage = {
                    status: 200,
                    message: "song updated",
                    data: song,
                };
                res.status(outputResponse.status).json(outputResponse);
            }
        })
    )
    // Deletes a song by ID
    .delete(
        raw(async (req: Request, res: Response) => {
            const song = await deleteSongByID(req.params.id);
            if (!song) {
                throw new HttpException(
                    400,
                    `No such song with id: ${req.params.id}`
                );
            } else {
                const outputResponse: IResponseMessage = {
                    status: 200,
                    message: "song deleted",
                    data: song,
                };
                res.status(outputResponse.status).json(outputResponse);
            }
        })
    );

export default router;
