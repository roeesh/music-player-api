import pkg from "bluebird";
import { ISong } from "../../../types/types.js";
import artist_model from "../artist/artist.model.js";
import {
    deleteArtistSongsByArtistID,
    updateArtistSongsByArtistID,
} from "../artist/artist.service.js";
import {
    deletePlaylistOfSongByPlaylistID,
    updatePlaylistOfSongByPlaylistID,
} from "../playlist/playlist.service.js";
import song_model from "./song.model.js";

const { Promise } = pkg;
export const NO_OWNER = "no such owner";

// Create new song (and insert it to its owner songs list)
export async function createNewSong(
    reqBody: ISong,
    ownerID: string
): Promise<any> {
    const artistOwner = await artist_model.findById(ownerID);
    const song = await song_model.create(reqBody);

    // insert to artist songs list
    artistOwner.songs.push(song);
    await artistOwner.save();
    return song;
}

// Get all songs
export async function getAllASongs(): Promise<any> {
    const songs = await song_model.find().select(`-_id 
                                                name 
                                                time
                                                owner
                                                playlists
                                                lyrics`);
    return songs;
}

// Get all songs of an artist by his ID
export async function getArtistsSongsByID(artistID: string): Promise<any> {
    const songsOfArtist = await artist_model
        .findById(artistID)
        .populate("songs").select(`-_id 
                                                                                        name 
                                                                                        time
                                                                                        owner 
                                                                                        playlists
                                                                                        lyrics`);
    return songsOfArtist;
}

// Get a single song
export async function getSongByID(idToRead: string): Promise<any> {
    const song = await song_model.findById(idToRead);
    return song;
}

// Update a song using the id
export async function updateSong(
    idToUpdate: string,
    reqBody: ISong
): Promise<any> {
    const oldSong = await song_model.findByIdAndUpdate(idToUpdate, reqBody, {
        new: false,
        upsert: false,
    });
    const song = await song_model.findByIdAndUpdate(idToUpdate, reqBody, {
        new: true,
        upsert: false,
    });
    if (song === null) {
        return undefined;
    }
    // update song in Artist.songs
    await updateArtistSongsByArtistID(song.owner, idToUpdate, song);

    // update song in each playlist in which the song in (Playlist.songs)
    Promise.each(song.playlists, async (playlistID: string) => {
        await updatePlaylistOfSongByPlaylistID(
            playlistID,
            idToUpdate,
            song,
            oldSong
        );
    });
    return song;
}

// Delete a song using the id
export async function deleteSongByID(idToDelete: string): Promise<any> {
    const song = await song_model.findByIdAndRemove(idToDelete);
    if (song === null) {
        return undefined;
    }
    // delete song in Artist.songs
    await deleteArtistSongsByArtistID(song.owner, idToDelete);

    // delete song in each playlist in which the song in (Playlist.songs)
    Promise.each(song.playlists, async (playlistID: string) => {
        await deletePlaylistOfSongByPlaylistID(playlistID, idToDelete, song);
    });
    return song;
}

// **** helper functions for playlist service ****

// delete playlist from each song in which the playlist in (Song.Playlists)
export async function deletePlaylistFromSong(
    playlistIDToDelete: string,
    song: ISong
) {
    const songFromPlaylist = await song_model.findById(song.id);
    if (songFromPlaylist !== null) {
        const playlistIndexToDelete: number =
            songFromPlaylist.playlists?.findIndex(
                (playlistID: string) => playlistID === playlistIDToDelete
            );
        songFromPlaylist.playlists.splice(playlistIndexToDelete, 1);
        await songFromPlaylist.save();
    }
}
